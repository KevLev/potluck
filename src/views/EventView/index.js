var React = require('react');
var ReactDOM = require('react-dom');
var helpers = require('../../helpers');
var SocialButton = require('../SocialButton')
import { History } from 'react-router'


var http = require("http");
var url = require("url");

let ClickOutHandler = require('react-onclickout');
import clipboard from 'clipboard-js';

// If you want to use a store for this view, you first load up a store:
//
//     var FooStore = require('../../stores/FooStore');
//
// You would then get the mixin to add functionality to our view that will best
// interact with our store:
//
//     var Fluxxor = require('Fluxxor');
//     
//     var StoreWatchMixin = Fluxxor.StoreWatchMixin;


var GeoCodeCalc = {};
GeoCodeCalc.EarthRadiusInMiles = 3956.0;
GeoCodeCalc.EarthRadiusInKilometers = 6367.0;
GeoCodeCalc.ToRadian = function(v) { return v * (Math.PI / 180);};
GeoCodeCalc.DiffRadian = function(v1, v2) { 
return GeoCodeCalc.ToRadian(v2) - GeoCodeCalc.ToRadian(v1);
};
GeoCodeCalc.CalcDistance = function(lat1, lng1, lat2, lng2, radius) { 
return radius * 2 * Math.asin( Math.min(1, Math.sqrt( ( Math.pow(Math.sin((GeoCodeCalc.DiffRadian(lat1, lat2)) / 2.0), 2.0) + Math.cos(GeoCodeCalc.ToRadian(lat1)) * Math.cos(GeoCodeCalc.ToRadian(lat2)) * Math.pow(Math.sin((GeoCodeCalc.DiffRadian(lng1, lng2)) / 2.0), 2.0) ) ) ) );
};

const EventView = React.createClass({

  // To hook a store up to this view, you would add the FluxMixin and
  // StoreWatchMixin:
  //
  //     mixins: [ helpers.FluxMixin, StoreWatchMixin('FooStore') ]
  //
  // Next, you would add a method that gets the state from the store.
  //
  //     getStateFromFlux: function () {
  //       return this.getFlux().store('FooStore').getState();
  //     },
  mixins: [ History ],
  focusChanged: function(is_focus){
    const {picture} = this.props.event_info;
    let video_element = ReactDOM.findDOMNode(this.refs['event-video']);
    if (is_focus == this.is_focus)
      return;
    this.is_focus = is_focus;
    if(is_focus)
    {
      if(!this.state.isLoaded){
        this.setState({loadedPercentage: 0});
        this.onProgressLoadTimer = setInterval(this.loadProgress, 30);
      }
      if(video_element.src!=''){
        video_element.play();
      }else{
        var parser = document.createElement('a');
        parser.href = picture['_url'];
        var options = {
          host: parser.host,
          path: parser.pathname,
        };
        var req = http.get(options,  (res) => {
          if (res.statusCode !== 200) {
            // handle error
            return;
          }
          var data = [], dataLen = 0;
          res.on("data", (chunk) => {
            data.push(chunk);
            dataLen += chunk.length;
          });

          res.on("end", () => {
            var buf = new Buffer(dataLen);
            for (var i=0,len=data.length,pos=0; i<len; i++) {
              data[i].copy(buf, pos);
              pos += data[i].length;
            }
            var blobObject = new Blob([new Uint8Array(buf)], { type: 'video/mp4' }); 
            var src = window.URL.createObjectURL(blobObject);
            video_element.src = src;
            video_element.load();
            this.setState({isLoaded: true});
            clearInterval(this.onProgressLoadTimer);
            if(this.is_focus)
              video_element.play();
          });
        });
      }
    }
    else{
      let video_element = ReactDOM.findDOMNode(this.refs['event-video']);
      video_element.pause();
      clearInterval(this.onProgressLoadTimer);
      this.setState({loadedPercentage: 0});
      
    }
  },

  onVidePlayable: function(){
    let video_element = ReactDOM.findDOMNode(this.refs['event-video']);
    //video_element.play();
  },

  loadProgress: function(){
    let newLoadedPercentage = this.state.loadedPercentage;
    if(newLoadedPercentage > 50)
      newLoadedPercentage += (98-newLoadedPercentage)/150;
    else
      newLoadedPercentage+=0.3;
    this.setState({loadedPercentage: newLoadedPercentage});
  },

  getInitialState: function() {

    return {
      'overlay': false,
      'like': false,
      'more_dropdown': false,
      'mute': false,
      'copy_link': false,
      'loadedPercentage': 0,
      'isLoaded': false
    };

  },

  onSoundOnOff: function(on){
    let video_element = this.refs['event-video'];
    video_element.muted = !on;
    this.setState({'mute': !on});
  },

  componentWillUnmount: function(){
    clearInterval(this.onProgressLoadTimer);
  },

  componentDidMount: function(){
    const {event_info} = this.props;
    let video_element = ReactDOM.findDOMNode(this.refs['event-video']);
    video_element.onprogress = () => {
      this.setState({loadedPercentage: 100});
      setTimeout(() => {this.setState({isLoaded: true}); clearInterval(this.onProgressLoadTimer);}, 300);
    };
  },

  onVideoBoxClick: function() {
    let video_element = ReactDOM.findDOMNode(this.refs['event-video']);
    if (video_element.paused && this.is_focus)
    {
      video_element.play();
    }else{
      video_element.pause();
    }
  },

  onVideoPlay: function() {
    let video_element = ReactDOM.findDOMNode(this.refs['event-video']);
    video_element.play();
    this.setState({'overlay': false});
  },

  onClickSharing: function(){
    this.setState({'overlay': true});
    this.onShareViewReset();
  },

  onStopSharing: function(){
    this.setState({'overlay': false});
  },

  onShareViewReset: function(){
    this.onShareViewClear();
    this.share_view_timeout = setTimeout(this.onStopSharing, 2000);
  },

  onShareViewClear: function(){
    if(this.share_view_timeout)
      clearTimeout(this.share_view_timeout);
  },

  onClickOutMoreDropdown: function(){
    this.setState({'more_dropdown': false});
  },
  
  onClickMoreDropdown: function(){
    this.setState({'more_dropdown': true});
  },

  onSoundOnOff: function(){
    let video_element = ReactDOM.findDOMNode(this.refs['event-video']);
    video_element.muted = !this.state.mute;
    this.setState({'mute': !this.state.mute});
  },

  onCopyLink: function(){
    clipboard.copy(location.protocol + "//" + location.host + "/" + this.props.event_info.objectId);
    this.setState({'more_dropdown': false, 'copy_link': true});
    setTimeout(this.onCopyLinkHidden, 1000);
  },

  onCopyLinkHidden: function(){
    this.setState({'copy_link':false});
  },

  onViewPost: function(){
    this.history.pushState(null, '/'+this.props.event_info.objectId);
  },

  onEmbed: function(){

    if(this.props.onEmbed)
      this.props.onEmbed();
    else
      this.history.pushState(null, '/'+this.props.event_info.objectId+'/embed');
  },

  millisecondsToStr: function  (milliseconds) {
    // TIP: to find current time in milliseconds, use:
    // var  current_time_milliseconds = new Date().getTime();

      function numberEnding (number) {
          return (number > 1) ? 's' : '';
      }

      var temp = Math.floor(milliseconds / 1000);
      var years = Math.floor(temp / 31536000);
      if (years) {
          return years + ' year' + numberEnding(years);
      }
      //TODO: Months! Maybe weeks? 
      var days = Math.floor((temp %= 31536000) / 86400);
      if (days) {
          return days + ' day' + numberEnding(days);
      }
      var hours = Math.floor((temp %= 86400) / 3600);
      if (hours) {
          return hours + ' hr' + numberEnding(hours);
      }
      var minutes = Math.floor((temp %= 3600) / 60);
      if (minutes) {
          return minutes + ' min' + numberEnding(minutes);
      }
      var seconds = temp % 60;
      if (seconds) {
          return seconds + ' sec' + numberEnding(seconds);
      }
      return 'just now'; //'just now' //or other string you like;
  },



  render: function () {
    if(this.props.event_info.code == 101){
      return(
        <div className={'event-view'}  onMouseEnter={this.onMouseEventEnter} onMouseLeave={this.onMouseEventLeave}>
          <div className='event-header'>
            <img className='event-avatar event-avatar-border'/>
            <div className='event-owner-info'>
              <div className='event-owner-name'>{name}</div>
              <div className='event-time-and-distance'></div>
            </div>
            <div className='event-vote'>+0</div>
          </div>
          
          <div className='event-video-box'>
            <div className='event-video event-video-failed'>
              <span>
                Video not found.
              </span>
            </div>
          </div>

          <div className='event-footer'>
            <div className='event-description'>
              Video ID looks not correct.
              Please check your url again.
            </div>
            <div className='event-footer-seperator'/>
            <div className='event-footer-buttons'>
              <div className='event-heart-group'>
                <div className='event-heart'>
                  <img src="/assets/img/heart.png" className='event-heart-image hidden'/>
                  <img src="/assets/img/heart_border.png" className='event-heart-border'/>
                </div>
              </div>
              <div className='event-comment-group'>
                <div className='event-comment'>
                  <img src="/assets/img/comment_icon.png" className='event-comment-icon'/>
                </div>
              </div>
              <div className='event-share'>
                <img src="/assets/img/share_icon.png" className='event-share-icon'/>
              </div>
              <div className='event-more'>
                <img src="/assets/img/more_icon.png" className='event-more-icon'/>
              </div>
            </div>
          </div>
        </div>
      );
    }

    const {overlay, like, more_dropdown, mute, copy_link} = this.state;
    const {thumbnail, crewList, caption, likes, commentCount, createdAt, location, picture} = this.props.event_info;
    const {profilePic, name} = this.props.event_info.user.attributes || this.props.event_info.user;
    const passed_period = this.millisecondsToStr(Date.now()-Date.parse(createdAt));
    const {event_info} = this.props;
    const {current_loc} = this.props;

    let distanceInMiles = GeoCodeCalc.CalcDistance(location.latitude, location.longitude, current_loc.latitude, current_loc.longitude, GeoCodeCalc.EarthRadiusInMiles); 
    const distance = parseInt(distanceInMiles);
    let event_overlay_className = overlay?'event-social-overlay':'event-social-overlay hidden';
    let event_heart_className = like?'event-heart-image':'event-heart-image hidden';
    let more_dropdown_className = more_dropdown?'event-more-dropdown': 'event-more-dropdown hidden';
    let copy_link_className = copy_link?'link-copied': 'link-copied hidden';
    //let event_overlay_className ='event-video-overlay';
    let play_pause_button_url = '/assets/img/play_button.png';
    let sound_icon_src = mute?'/assets/img/sound-off.png':'/assets/img/sound-on.png';

    return (

      <div className='event-view'  onMouseEnter={this.onMouseEventEnter} onMouseLeave={this.onMouseEventLeave}>
        <div className='event-header'>
          <img className='event-avatar' src={profilePic['_url']}  onClick={this.props.onClickRestricted}/>
          <div className='event-owner-info'>
            <div className='event-owner-name' onClick={this.props.onClickRestricted}>{name}</div>
            <div className='event-time-and-distance'>{passed_period} • {distance} miles</div>
          </div>
          <div className='event-vote' onClick={this.props.onClickRestricted}>+{crewList.length}</div>
        </div>
        
        <div className='event-video-box' onClick={this.onVideoBoxClick}>
          <video ref='event-video' poster={thumbnail['_url']} loop preload='auto' className='event-video'>
            Your browser does not support the video tag.
          </video>
          <img className='sound-onoff' src={sound_icon_src} onClick={this.props.onSoundOnOff}/>
          <img className={copy_link_className} src='/assets/img/link-copied.png'/>
          {this.state.isLoaded?null:<div className = 'buffering-progress-bar' style={{'width': this.state.loadedPercentage+'%'}}></div>}
          <div className={event_overlay_className} onMouseEnter={this.onShareViewClear} onMouseLeave={this.onShareViewReset}>
            <SocialButton share_info={{
                type: "facebook",
                url: "http://potluckapp.net/"+event_info.objectId,
                //url: "http://localhost:3000/"+event_info.objectId,
                title: "Potluck Video",
                descr: caption,
                image: thumbnail['_url'],
                source: picture['_url'],
                winWidth: 500,
                winHeight: 500
              }} src="/assets/img/social-facebook.png" title="Facebook" style={{left: '12.5%', top: '40%', width: '13%'}}/>
            <SocialButton share_info={{
                type: "twitter",
                url: "http://potluckapp.net/"+event_info.objectId,
                //url: "http://localhost:3000/"+event_info.objectId,
                title: "Potluck Video",
                descr: caption,
                image: thumbnail['_url'],
                source: picture['_url'],
                winWidth: 500,
                winHeight: 500
              }}
              src="/assets/img/social-twitter.png" title="Twitter" style={{left: '37.5%', top: '40%', width: '13%'}}/>
            <SocialButton share_info={{
                type: "reddit",
                url: "http://potluckapp.net/"+event_info.objectId,
                //url: "http://localhost:3000/"+event_info.objectId,
                title: "Potluck Video",
                descr: caption,
                image: thumbnail['_url'],
                source: picture['_url'],
                winWidth: 500,
                winHeight: 500
              }} src="/assets/img/social-reddit.png" title="Reddit" style={{left: '62.5%', top: '40%', width: '13%'}}/>
            <SocialButton share_info={{
                type: "tumblr",
                url: "http://potluckapp.net/"+event_info.objectId,
                //url: "http://localhost:3000/"+event_info.objectId,
                title: "Potluck Video",
                descr: caption,
                image: thumbnail['_url'],
                source: picture['_url'],
                winWidth: 500,
                winHeight: 500
              }} src="/assets/img/social-tumblr.png" title="Tumblr" style={{left: '87.5%', top: '40%', width: '13%'}}/>
          </div>
        </div>

        <div className='event-footer'>
          <div className='event-description'>
            {caption}
          </div>
          <div className='event-footer-seperator'/>
          <div className='event-footer-buttons'>
            <div className='event-heart-group' onClick={this.props.onClickRestricted}>
              <div className='event-heart'>
                <img src="/assets/img/heart.png" className={event_heart_className}/>
                <img src="/assets/img/heart_border.png" className='event-heart-border'/>
              </div>
            </div>
            <div className='event-comment-group' onClick={this.props.onClickRestricted}>
              <div className='event-comment'>
                <img src="/assets/img/comment_icon.png" className='event-comment-icon'/>
              </div>
            </div>
            <div className='event-share'>
              <img src="/assets/img/share_icon.png" className='event-share-icon' onClick={this.onClickSharing}/>
            </div>
            <div className='event-more'>
              <ClickOutHandler onClickOut={this.onClickOutMoreDropdown}>
                <img src="/assets/img/more_icon.png" className='event-more-icon' onClick={this.onClickMoreDropdown}/>
                <div className={more_dropdown_className}>
                  <div className='event-more-menu-item' onClick={this.onViewPost}>View post</div>
                  <div className='event-more-menu-item' onClick={this.onEmbed}>Embed</div>
                  <div className='event-more-menu-item' onClick={this.onCopyLink}>Copy link</div>
                </div>
              </ClickOutHandler>
            </div>
          </div>
        </div>
      </div>
    );
  }

});

module.exports = EventView;
