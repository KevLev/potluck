var React = require('react');

// If you want to use a store for this view, you first load up a store:
//
//     var FooStore = require('../../stores/FooStore');
//
// You would then get the mixin to add functionality to our view that will best
// interact with our store:
//
//     var Fluxxor = require('Fluxxor');
//     
//     var StoreWatchMixin = Fluxxor.StoreWatchMixin;

module.exports = React.createClass({

  getInitialState: function() {

    return {
      
    };
  },

  render: function () {
    return (
      <div className='link-expired'>
        <div className='title'>Link Expired</div>
        <div className='description'>
          Events on Potluck only last 24 hours. <br/>
          Please download the App to see the <br/>
          latest events in your area. <br/>
        </div>
        <img src='/assets/img/appstore-download-green.png' className='appstore-download'/>
        <img src='/assets/img/link-expired-monkey.png' className='link-expired-monkey'/>
      </div>
    );
  }
});
