var React = require('react');
var helpers = require('../../helpers');

// If you want to use a store for this view, you first load up a store:
//
//     var FooStore = require('../../stores/FooStore');
//
// You would then get the mixin to add functionality to our view that will best
// interact with our store:
//
//     var Fluxxor = require('Fluxxor');
//     
//     var StoreWatchMixin = Fluxxor.StoreWatchMixin;

module.exports = React.createClass({

	onSocialButtonClick: function(e){
		e.stopPropagation();
	},

  getDefaultProps: function() {
    return {
      share_info: {}
    };
  },

  socialShare: function(type, url, title, descr, image, source, winWidth, winHeight) {
    var winTop = (screen.height / 2) - (winHeight / 2);
    var winLeft = (screen.width / 2) - (winWidth / 2);
    switch(type){
      case "facebook":
        FB.ui({
          method: 'share',
          href:  url,
          name: title,
          caption: title,
          description: descr,
          picture: image,
          source: source,
          type: "video/mp4"
        }, function(response){});

        //window.open('http://www.facebook.com/sharer.php?u='+url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        //window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(url), 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        break;
      case "twitter":
        window.open('https://twitter.com/share?url='+encodeURIComponent(url)+'&text='+encodeURIComponent(descr), 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        break;
      case "tumblr":
        window.open('http://www.tumblr.com/share/photo?source='+encodeURIComponent(url)+'&caption='+encodeURIComponent(descr)
          +'&click_thru='+encodeURIComponent(url), 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        break;
      case "reddit":
        window.open('http://reddit.com/submit?url='+encodeURIComponent(url)+'&title='+encodeURIComponent(descr), 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        break;
    }
  },
  //<a href = {"javascript:fbShare('"+type+"','"+url+"','"+title+"','"+descr+"','"+image+"','"+winWidth+"','"+winHeight+"')"}>
  render: function () {
    const {type, url, title, descr, image, source, winWidth, winHeight} = this.props.share_info;
    return (
      <div className='social-button' style={this.props.style} onClick={this.onSocialButtonClick}>
        <a onClick={() => this.socialShare(type,url,title,descr,image,source,winWidth,winHeight)}>
          <img src={this.props.src} className='social-button-icon'/>
          <div className='social-button-title'>{this.props.title}</div>
        </a>
      </div>
    );
  }
});
