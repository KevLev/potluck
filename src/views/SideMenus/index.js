var React = require('react');
var helpers = require('../../helpers');
import { History } from 'react-router';
// If you want to use a store for this view, you first load up a store:
//
//     var FooStore = require('../../stores/FooStore');
//
// You would then get the mixin to add functionality to our view that will best
// interact with our store:
//
//     var Fluxxor = require('Fluxxor');
//     
//     var StoreWatchMixin = Fluxxor.StoreWatchMixin;

module.exports = React.createClass({
  mixins: [ History ],
  onClickContact: function(){
    this.history.pushState(null, '/contact');
  },

  onClickTerms: function(){
    this.history.pushState(null, '/term');
  },

  onClickPolicy: function(){
    this.history.pushState(null, '/policy');
  },

  onClickGeofilters: function(){
    this.history.pushState(null, '/geofilter');
  },

  render: function () {

    return (
      <div className={'side-menu ' + this.props.className}>
        <div className='twitter-menu'>
          <a href="https://twitter.com/OfficialPotluck/" target="_blank">
            <img src='/assets/img/twitter_menu.png'/>
          </a>
        </div>
        <div  className='facebook-menu'>
          <a href="https://www.facebook.com/OfficialPotluck/" target="_blank">
            <img src='/assets/img/facebook_menu.png'/>
          </a>
        </div>
        <div className='contact' onClick={this.onClickContact}>Contact</div>
        <div className='terms' onClick={this.onClickTerms}>Terms</div>
        <div className='policy' onClick={this.onClickPolicy}>Policy</div>
        <div className='geofilters' onClick={this.onClickGeofilters}>Geofilters</div>
        <span>©  Potluck <br/>
          Group LLC</span>
      </div>
    );
  }
});
