var React = require('react');
var helpers = require('../../helpers');

import {ScrollListenerMixin} from 'react-scroll-components';
import { History } from 'react-router';
import SideMenus from '../SideMenus';
var http = require("http");
var url = require("url");
var Switch = require('rc-switch');
// If you want to use a store for this view, you first load up a store:
//
//     var FooStore = require('../../stores/FooStore');
//
// You would then get the mixin to add functionality to our view that will best
// interact with our store:
//
//     var Fluxxor = require('Fluxxor');
//     
//     var StoreWatchMixin = Fluxxor.StoreWatchMixin;

module.exports = React.createClass({
  
  mixins: [ScrollListenerMixin, History],

  getInitialState: function() {

    return {
      'mute': this.props.mute
    };

  },

  componentDidMount: function() {
    const {picture, objectId} = this.props.event_info;
    const {protocol, host} = window.location;
    let video_element = this.refs['event-video'];
    var parser = document.createElement('a');
    parser.href = picture['_url'];
    var options = {
      host: parser.host,
      path: parser.pathname,
    };
    var req = http.get(options,  (res) => {
      if (res.statusCode !== 200) {
        // handle error
        return;
      }
      
      var data = [], dataLen = 0;
      res.on("data", (chunk) => {
        data.push(chunk);
        dataLen += chunk.length;
      });

      res.on("end", () => {
        var buf = new Buffer(dataLen);
        for (var i=0,len=data.length,pos=0; i<len; i++) {
          data[i].copy(buf, pos);
          pos += data[i].length;
        }

        var blobObject = new Blob([new Uint8Array(buf)], { type: 'video/mp4' }); 

        var src = window.URL.createObjectURL(blobObject);
        video_element.src = src;
        video_element.load();
        video_element.play();
        video_element.muted = true;
      });
    });
  },

  onSoundOnOff: function(on){
    let video_element = this.refs['event-video'];
    video_element.muted = !on;
    this.setState({'mute': !on});
  },

  onSwitch: function(is_on) {
    console.log(is_on);
  },

  onEmbedStringChange: function(e){
    this.setState({embed: e.target.value});
  },
  
  render: function () {
    const {mute} = this.state;
    const {thumbnail, objectId} = this.props.event_info;
    const {origin} = window.location;
    let sound_icon_src = mute?'/assets/img/sound-off.png':'/assets/img/sound-on.png';

    return (
      <div className='embed-view'>
        <div className='video-wrapper'>
          <div className='title'>Potluck</div>
          <div className='event-video-box' onClick={this.onVideoBoxClick}>
            <video ref='event-video' poster={thumbnail['_url']} loop preload='auto' className='event-video'>
              Your browser does not support the video tag.
            </video>
            <img className='sound-onoff' src={sound_icon_src} onClick={this.props.onSoundOnOff}/>
          </div>
        </div>
        <div className='footer'>
          <div className='controls'>
            <span>Embed Code</span>
            <span>Auto Play: </span>
            <Switch onChange={this.onSwitch}/>
          </div>
          <textarea rows="4" cols="50" className='textarea' defaultValue={'<iframe src="'+origin+"/"+objectId+'/embed/simple" width="600" height="600" frameborder="0"></iframe><script src=""></script>'}>
          
          </textarea>
        </div>
      </div>
    );
  }
});
