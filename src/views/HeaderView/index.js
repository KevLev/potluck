var React = require('react');
var helpers = require('../../helpers');

import {ScrollListenerMixin} from 'react-scroll-components';
import { History } from 'react-router';
import SideMenus from '../SideMenus';

// If you want to use a store for this view, you first load up a store:
//
//     var FooStore = require('../../stores/FooStore');
//
// You would then get the mixin to add functionality to our view that will best
// interact with our store:
//
//     var Fluxxor = require('Fluxxor');
//     
//     var StoreWatchMixin = Fluxxor.StoreWatchMixin;

module.exports = React.createClass({
  
  mixins: [ScrollListenerMixin, History],

  getInitialState: function() {

    return {
      'header_fixed': this.props.className=='fixed' ? true : false
    };
  },

  onPageScroll: function(scroll_pos){
    var stand_scroll_pos = 330;
    if(this.props.className=='fixed')
      return;
    if(scroll_pos > stand_scroll_pos && !this.state.header_fixed){
      this.setState({'header_fixed': true});
    }else if(scroll_pos <= stand_scroll_pos && this.state.header_fixed){
      this.setState({'header_fixed': false});
    }
  },

  onClickHome: function(){
    this.history.pushState(null, '/');
  },

  render: function () {

    let headerClassName = this.state.header_fixed?'header-view fixed':'header-view';
    let fixedHeaderClassName = this.state.header_fixed?'fixed-header-view':'fixed-header-view hidden';

    return (
      <div className={'header-view' + ' ' + this.props.className}>
        <div className='header-original-view'>
          <img className='header-logo-icon' src='/assets/img/logo.png'/>
          <div className='header-logo'>
            Potluck
          </div>
          <div className='header-description'>
            Discover social events near you <br/>
            through pictures and videos
          </div>
          <a href='#' className='appstore-download'>
            <img src='/assets/img/appstore-download.png'/>
          </a>
          <div className='title'>
            <div className = 'stroke'/>
            {this.props.title}
          </div>
          <SideMenus className={this.state.header_fixed?'fixed':''}/>
        </div>
        <div className={fixedHeaderClassName}>
          <img className='header-logo-icon' src='/assets/img/logo.png' onClick={this.onClickHome}/>
          <div className='header-logo' onClick={this.onClickHome}>
            Potluck
          </div>
        </div>
      </div>
    );
  }
});
