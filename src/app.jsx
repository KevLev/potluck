var React = require('react');
var ReactDOM = require('react-dom');
var History = require('history');
var createBrowserHistory = require('history/lib/createBrowserHistory');
import {Router, IndexRoute, Route} from 'react-router';

var mui = require('material-ui');
var injectTapEventPlugin = require("react-tap-event-plugin");

import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
//import DevTools from './containers/DevTools';

const store = configureStore();

// A lot of the code is auto-generated. However, fiddling around with it
// shouldn't be a catastrophic failure. Just that you'd need to know your way
// around a little. However, **BE CAREFUL WHILE DELETING SOME OF THE COMMENTS IN
// THIS FILE; THE AUTO-GENERATORS RELY ON SOME OF THEM**.

// inject:pagerequire
var NearbyPage = require('./pages/NearbyPage');
var EventPage = require('./pages/EventPage');
var EmbedSimplePage = require('./pages/EmbedSimplePage');
var ContactPage = require('./pages/ContactPage');
var ContactSuccessPage = require('./pages/ContactSuccessPage');
var TermPage = require('./pages/TermPage');
var PolicyPage = require('./pages/PolicyPage');
var GeofilterPage = require('./pages/GeofilterPage');
var GeofilterUploadPage = require('./pages/GeofilterUploadPage');
var GeofilterUploadSuccessPage = require('./pages/GeofilterUploadSuccessPage');
var AppLinksPage = require('./pages/AppLinksPage');

var GeofilterUploadPageFreeWrapper = React.createClass({
  render: function () {
    return (
        <GeofilterUploadPage type="free" />
    );
  }
});

var GeofilterUploadPageAdvertiseWrapper = React.createClass({
  render: function () {
    return (
        <GeofilterUploadPage type="advertise" />
    );
  }
});

var EventPageWrapper = React.createClass({
  render: function () {
    return (
        <EventPage embed={false} objectId = {this.props.params.objectId}/>
    );
  }
});

var EventEmbedPageWrapper = React.createClass({
  render: function () {
    return (
        <EventPage embed={true} objectId = {this.props.params.objectId}/>
    );
  }
});




// endinject
import react from 'react';

var AppCanvas = mui.AppCanvas;
var AppBar = mui.AppBar;
var LeftNav = mui.LeftNav;
injectTapEventPlugin();

var Master = React.createClass({
  mixins: [Router.State],

  render: function () {
    return (
      <div>
        {this.props.children}
      </div>
    );
  }
});

var routes = [
  <Route path='/' component={Master}>
    {/* inject:route */}
    <IndexRoute component={NearbyPage} />
    <Route path='/applinks' component={AppLinksPage} />
    <Route path='/contact' component={ContactPage} />
    <Route path='/contact-success' component={ContactSuccessPage} />
    <Route path='/term' component={TermPage} />
    <Route path='/policy' component={PolicyPage} />
    <Route path='/geofilter' component={GeofilterPage}/>
    <Route path='/geofilter-free' component={GeofilterUploadPageFreeWrapper}/>
    <Route path='/geofilter-advertise' component={GeofilterUploadPageAdvertiseWrapper}/>
    <Route path='/geofilter-submit-success' component={GeofilterUploadSuccessPage}/>
    <Route path= "/:objectId" component={EventPageWrapper}/>
    <Route path= "/:objectId/embed" component={EventEmbedPageWrapper}/>
    <Route path= "/:objectId/embed/simple" component={EmbedSimplePage}/>
    
    {/* endinject */}
  </Route>
];

ReactDOM.render(<Provider store={store}>
    <div>
      <Router history={createBrowserHistory()}>{routes}</Router>
    </div>
  </Provider>
  , document.getElementById('reactBody'));
