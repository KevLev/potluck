var React = require('react');
import Header from '../../views/HeaderView';
import BottomMenus from '../../views/BottomMenus';
var ReactDOM = require('react-dom');
import { History } from 'react-router';

// If you are going to be using stores, be sure to first load in the `Fluxxor`
// module.
//
//     var Fluxxor = require('Fluxxor');
//
// If you want to leverage the use of stores, a suggestion would be to
// initialize an object, and set it to a `stores` variable, and adding a new
// instance of the store as a property to the object, like so:
//
//     var stores = {
//       SomeStore: new SomeStore()
//     };
//
// And also, because we are using the Flux architecture, you may also initialize
// an object full of methods that represent "actions" that will be called upon
// by a "dispatcher", like so:
//
//     var actions = {
//       doSomething: function (info) {
//         this.dispatch('DO_SOMETHING', {info: info});
//       }
//     };
//
// And finally, you would pass the stores and actions to our dispatcher, like
// so:
//
//     var flux = new Fluxxor.Flux(stores, actions);
//
// And, then, you would pass in the reference of your dispatcher to the view
// relies on the dispatcher (that view is returned by the `render` method), like
// so:
//
//     <SomeView flux={flux} />

module.exports = React.createClass({

  mixins: [ History ],
  onClickUploadFree: function(){
    this.history.pushState(null, '/geofilter-free');
  },

  onClickUploadAdvertise: function(){
    this.history.pushState(null, '/geofilter-advertise');
  },

  render: function () {

    let contact_success = 
      <div className='full-screen'>
        <div className='center-page'>
          <div className='geofilter-body'>
            <img src='/assets/img/geofilter_title.png' className='advertise-title'></img>
            <div className='description'>Geofilters are a unique overlay over pictures and videos to bring creativity to 
                            your town, area, or business. We currently offer two options to add geofilters 
                            to Potluck. All filters must be approved by the Potluck team.</div>
            <div className='main-area'>
              <div className='membership-area'>
                <div className='membership-title'>Free</div>
                <div className='membership-button' onClick={this.onClickUploadFree}>Upload</div>
                <div className='membership-description'>For users to upload filters for their area</div>
              </div>
              <div className='membership-area'>
                <div className='membership-title'>$30/mo</div>
                <div className='membership-button' onClick={this.onClickUploadAdvertise}>
                  Advertise
                </div>
                <img className='three-months-free' src='/assets/img/3-months-free.png'/>
                <div className='membership-description'>For businesses to advertise</div>
              </div>
            </div>
            <div className='download-area'>
              <div className='title'>Download</div>
              <a href="/assets/img/GeofilterTemplate.ai" className='download-button'>Illustrator Template</a>
              <a href="/assets/img/GeofilterTemplate.psd" className='download-button'>Photoshop Template</a>
            </div>
          </div>
        </div>
      </div>
    return (
      <div className='geofilter-page'>
        <Header className='fixed'/>
        {contact_success}
        <BottomMenus/>
      </div>
    );
  }

});
