var React = require('react');
import Header from '../../views/HeaderView';
var ReactDOM = require('react-dom');
import Slider from 'rc-slider';
import Parse from 'parse';
import { History } from 'react-router';
import Loader from 'react-loader';
// If you are going to be using stores, be sure to first load in the `Fluxxor`
// module.
//
//     var Fluxxor = require('Fluxxor');
//
// If you want to leverage the use of stores, a suggestion would be to
// initialize an object, and set it to a `stores` variable, and adding a new
// instance of the store as a property to the object, like so:
//
//     var stores = {
//       SomeStore: new SomeStore()
//     };
//
// And also, because we are using the Flux architecture, you may also initialize
// an object full of methods that represent "actions" that will be called upon
// by a "dispatcher", like so:
//
//     var actions = {
//       doSomething: function (info) {
//         this.dispatch('DO_SOMETHING', {info: info});
//       }
//     };
//
// And finally, you would pass the stores and actions to our dispatcher, like
// so:
//
//     var flux = new Fluxxor.Flux(stores, actions);
//
// And, then, you would pass in the reference of your dispatcher to the view
// relies on the dispatcher (that view is returned by the `render` method), like
// so:
//
//     <SomeView flux={flux} />

module.exports = React.createClass({

  mixins: [ History ],

  getInitialState: function() {
    return {
      'radius': 0.1,
      'submission_guideline_checked': false,
      'policy_checked': false,
      'pin_drop': false,
      'lat': null,
      'lng': null,
      'valid_png': null,
      'is_submitting': false,
    };
  },

  onSubmissionGuideCheck: function(){
    this.setState({'submission_guideline_checked': !this.state.submission_guideline_checked});
  },

  onPrivacyClick: function(){
    this.setState({'policy_checked': !this.state.policy_checked});
  },

  onSlideChange: function(value) {
    this.circle.setRadius(value*1609.3);
    this.setState({'radius': value});
  },

  componentDidMount: function(){
    const APPLICATION_ID = "BfwANODEExfcO23SduQLMdSpuXIIxrOloCm65EWi";
    const JAVASCRIPT_KEY = "94OyYy1qVBQz6clqPI7bzUHAl02oFtTUrN7KwfNU";
    Parse.initialize(APPLICATION_ID, JAVASCRIPT_KEY);
    this.map = new google.maps.Map(document.getElementById('google-map'), {
      center: {lat: CurrentLocation.latitude, lng: CurrentLocation.longitude},
      zoom: 10
    });

      // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');

    var searchBox = new google.maps.places.SearchBox(input);
    this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    this.map.addListener('bounds_changed', () => {
      searchBox.setBounds(this.map.getBounds()); 
    });
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', () => {
      var places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }

      // For each place, get the icon, name and location.
      var bounds = new google.maps.LatLngBounds();
      places.forEach(function(place) {
        var icon = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };

        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      this.map.fitBounds(bounds);
    });

    google.maps.event.addListener(this.map, "click", this.onGoogleMapClick);

  },

  onGoogleMapClick: function(event){
    if (!this.state.pin_drop)
      return;
    this.marker = new google.maps.Marker({
        position: event.latLng,
        map: this.map
    });
    this.circle = new google.maps.Circle({
      map: this.map,
      radius: this.state.radius*1609.3,    // 10 miles in metres
      fillColor: '#90cb9f',
      strokeColor: '#30cb30',
      strokeOpacity: 1,
      strokeWeight: 2,
      draggable: true
    });
    this.circle.bindTo('center', this.marker, 'position');
    this.setState({'lat': event.latLng.lat(), 'lng': event.latLng.lng(), 'pin_drop': false});
  },

  dropPinSelect: function(){
    if(this.marker){
      this.marker.setMap(null);
    }
    if(this.circle){
      this.circle.setMap(null);
    }
    this.setState({'pin_drop': true});
  },

  onUploadFileTrigger: function() {
    this.refs.uploadImage.click();
  },

  onChangeFile: function(e){
    var _URL = window.URL || window.webkitURL;
    var file, img;
    var _self = this;
    if ((file = _self.refs.uploadImage.files[0])) {
        img = new Image();
        img.onload = function () {
          if (this.width == 1080 && this.height == 1920)
          {
            _self.refs.picPreview.src = _URL.createObjectURL(file);
            _self.setState({valid_png: true});
          }else{
            _self.setState({valid_png: false});
          }
        };
        img.src = _URL.createObjectURL(file);
    }
  },

  onUpload: function(){
    var file = this.refs.uploadImage.files[0];
    var name = file.name; //This does *NOT* need to be a unique name
    
    var parseFile = new Parse.File(name, file);
    var lat = this.marker.getPosition().lat();
    var lng = this.marker.getPosition().lng();
    this.setState({is_submitting: true});
    var _self = this;
    parseFile.save().then(function() {
      var GeofilterApplication = new Parse.Object("Geofilter");
      GeofilterApplication.set("isFree", _self.props.type=='free'?true:false);
      GeofilterApplication.set("screenFile", parseFile);
      GeofilterApplication.set("description", _self.refs.description.value);
      GeofilterApplication.set("name", _self.refs.name.value);
      GeofilterApplication.set("email", _self.refs.email.value);
      GeofilterApplication.set("location", new Parse.GeoPoint(lat, lng));
      GeofilterApplication.set("radiusInMiles", _self.state.radius);
      GeofilterApplication.set("show", false);
      GeofilterApplication.save().then(
        function(){
          _self.setState({is_submitting: false});
          if(_self.props.type!='free')
          {
            //console.log(_self.refs.paypalButton);
            _self.refs.paypalForm.submit();

          }
          else
            _self.history.pushState(null, '/geofilter-submit-success');
        },
        function(error){
          console.log(error);
        }
      );
    }, function(error) {
      //error handling

    });
  },

 /* openPopupForPaypal: function(){
    var w = window.open('about:blank','Popup_Window','toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,'+
        'resizable=0,width=600,height=500,left = 500,top = 300');
  },
*/
  render: function () {
    const {submission_guideline_checked, policy_checked, valid_png} = this.state;
    let uploadValid = (submission_guideline_checked && policy_checked && valid_png && this.marker);
    let contact_form = 
      
        <div className='content'>
          <div className='header'>
            <div className='left'>
              <img className='logo-gray' src='/assets/img/logo-gray.png'/>
              <div className='title'>Geofilters</div>
              <div className='sub-title'>{this.props.type=='free'?'Upload':'Advertise'}</div>
            </div>
            <div className='right'>
              <div className='download'>Download</div>
              <a href="/assets/img/GeofilterTemplate.ai" className='download-button'>Illustrator Template</a>
              <a href="/assets/img/GeofilterTemplate.psd" className='download-button'>Photoshop Template</a>
            </div>
          </div>
          <div className='body'>
            <div className='google-map-area'>
              <div className={'google-map' + (this.state.pin_drop?' pin-drop-cursor':'')}  id='google-map'/>
              <input id="pac-input"  className="controls" type="text" placeholder="Search Box"></input>
              <div className={this.state.pin_drop?'drop-pin dropping':'drop-pin'} onClick={this.dropPinSelect}>
                <img src="/assets/img/drop-pin.png" className='drop-pin-img'/>
              </div>
            </div>
            <div className='side-window'>
              <div className='upload-box' onClick={this.onUploadFileTrigger}>
                <input type='file' hidden ref="uploadImage" accept="image/png" onChange={this.onChangeFile}/>
                <img className='plus' src='/assets/img/plus.png'/>
                <div className='description'>Upload a 1080 x 1920<br/>PNG file</div>
                <img className={'pic-preview' + (this.state.valid_png?' valid' : '')} ref="picPreview"/>
              </div>
              <div className='sliding-area'>
                <div className='radius-label'>Radius</div>
                <div className='radius'>{this.state.radius}mi</div>
                <Slider className='slider' min={0.1} max={this.props.type=="free"?30:1.0} step={0.1}  value={this.state.radius} tipTransitionName="rc-slider-tooltip-zoom-down" onChange={this.onSlideChange} />
              </div>
              <textarea className='description side-window-input' ref='description' placeholder='Description'></textarea>
              <textarea className='name side-window-input' ref='name' placeholder='Name' rows="1"></textarea>
              <textarea className='email side-window-input' ref='email' placeholder='Email' rows="1"></textarea>
              <div className='checkbox-area'>
                <div className='checkbox' onClick={this.onSubmissionGuideCheck}>
                  <img className={this.state.submission_guideline_checked?'checked checkbox-fill':' checkbox-fill'} src='/assets/img/checkbox-circle.png'/>
                </div>
                <div className='checkbox-description'>
                  {this.props.type == "free"?"All submitted content is original work that I've created. It doesn't contain logos or trademarks and I agree to the ":"All submitted content is original work that I've created. Any logos or trademarks are owned by me and I agree to the "}
                  <span className='checkbox-link'>Submission Guidlines</span>
                </div>
              </div>
              <div className='checkbox-area'>
                <div className='checkbox' onClick={this.onPrivacyClick}>
                  <img className={this.state.policy_checked?'checked checkbox-fill':' checkbox-fill'} src='/assets/img/checkbox-circle.png'/>
                </div>
                <div className='checkbox-description'>
                  {"I agree to the "}
                  <span className='checkbox-link'>Privacy Policy</span>
                  {" and "}<br/>
                  <span className='checkbox-link'>Submission Agreement</span>
                  {" of the"}<br/>
                  {"Potluck Geofilter tool "}
                </div>
              </div>
              <div className={'submit' + (uploadValid?' valid':'')} onClick={uploadValid?this.onUpload : null} >{this.props.type=='free'?'Submit':'Submit & Pay'}</div>
              <form ref="paypalForm" action="https://www.paypal.com/cgi-bin/webscr" method="post" target='_top'>
                <input type="hidden" name="cmd" value="_s-xclick"/>
                <input type="hidden" name="hosted_button_id" value="KYXLP756BJ97U"/>
                <input type="image" hidden ref="paypalButton" src="https://www.paypalobjects.com/en_US/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!"/>
                <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1"/>
              </form>
            </div>
          </div>
        </div>
    return (

      <div className='geofilter-upload-page'>
        <Header className='fixed'/>
        {contact_form}
        <Loader loaded={!this.state.is_submitting}> 
        </Loader>
      </div>
    );
  }

});
