var React = require('react');
import Header from '../../views/HeaderView';
import EventView from '../../views/EventView';
import EmbedView from '../../views/EmbedView';
import LinkExpiredView from '../../views/LinkExpiredView';
import BottomMenus from '../../views/BottomMenus';
import Parse from 'parse';
var ReactDOM = require('react-dom');
import Helmet from "react-helmet";
import { History, Router } from 'react-router';

import {ScrollListenerMixin} from 'react-scroll-components';

// If you are going to be using stores, be sure to first load in the `Fluxxor`
// module.
//
//     var Fluxxor = require('Fluxxor');
//
// If you want to leverage the use of stores, a suggestion would be to
// initialize an object, and set it to a `stores` variable, and adding a new
// instance of the store as a property to the object, like so:
//
//     var stores = {
//       SomeStore: new SomeStore()
//     };
//
// And also, because we are using the Flux architecture, you may also initialize
// an object full of methods that represent "actions" that will be called upon
// by a "dispatcher", like so:
//
//     var actions = {
//       doSomething: function (info) {
//         this.dispatch('DO_SOMETHING', {info: info});
//       }
//     };
//
// And finally, you would pass the stores and actions to our dispatcher, like
// so:
//
//     var flux = new Fluxxor.Flux(stores, actions);
//
// And, then, you would pass in the reference of your dispatcher to the view
// relies on the dispatcher (that view is returned by the `render` method), like
// so:
//
//     <SomeView flux={flux} />

module.exports = React.createClass({
  mixins: [ History ],
  getInitialState: function() {

    return {
      restrict_clicked: false,
      latitude: CurrentLocation.latitude,
      longitude: CurrentLocation.longitude,
      event: null,
      embed: false,
      embed_sound_mute: true
    };
  },
  
  onClickRestricted: function(){
    this.setState({
      restrict_clicked: true
    });
  },

  onClickOutsideRestrictedPopup: function(){
    this.setState({
      restrict_clicked: false
    });
  },

  onSoundOnOff: function(e){
    e.stopPropagation();
    const events = [{id: 1}, {id: 2}, {id: 3}];
    for (var i = 0; i < events.length; i++)
      this.refs['event-view'].onSoundOnOff();
  },

  onFocusChange: function(focus){
    this.refs['event-view'] && this.refs['event-view'].focusChanged(focus);
    this.setState({embed_sound_mute: focus});
  },

  onBlur: function(){
    this.onFocusChange(false);

  },

  onFocus: function(){
    this.onFocusChange(true);
  },

  componentDidMount: function(){
    this.onFocusChange(true);
    window.onblur = this.onBlur;
    window.onfocus = this.onFocus;
    if (this.props.embed && !this.state.embed){
      this.setState({embed: true});
    }
    if(this.props.event_info)
    {
      this.setState({event: this.props.event_info});
      return;
    }

    const APPLICATION_ID = "BfwANODEExfcO23SduQLMdSpuXIIxrOloCm65EWi";
    const JAVASCRIPT_KEY = "94OyYy1qVBQz6clqPI7bzUHAl02oFtTUrN7KwfNU";
    Parse.initialize(APPLICATION_ID, JAVASCRIPT_KEY);

    Parse.Cloud.run('Event', { id: this.props.objectId }, {
      success: (data) => {
        this.setState({
          event: data
        });
      },
      error: (error) => {
        this.setState({
          event: {code: 101}
        });
      }
    });
  },

  componentDidUpdate: function(){
    this.onFocusChange(true);
  },

  onCancelEmbed: function(e){
    if(!$(e.target).hasClass('embed-view-wrapper')) return;
    this.setState({embed: false});
    window.history.pushState(null, "Title", '/'+this.state.event.objectId);
  },

  onOpenEmbed: function(){
    this.setState({embed: true});
    window.history.pushState(null, "Title", '/'+this.state.event.objectId+'/embed');
  },

  render: function () {

    const {restrict_clicked, event} = this.state;
    let restrict_overlay = restrict_clicked 
      ?
        <div className='event-restrict-overlay'>
          <div className='overlay' onClick={this.onClickOutsideRestrictedPopup}/>
          <div className='popup'>
            <div className='title'>Uh Oh</div>
            <div className='description'>
              This feature is only available in the mobile app.<br/><br/>
              Please download the free Potluck App for full access.<br/><br/>
            </div>
            <img src='/assets/img/appstore-download-green.png' className='appstore-download'/>
            <img src='/assets/img/download-popup-close.png' className='close-button' onClick={this.onClickOutsideRestrictedPopup}/>
          </div>
        </div>
      :
        null; 
    if (restrict_clicked){
      document.body.style.overflow = "hidden";
    }else{
      document.body.style.overflow = "auto";
    }
    let events_view = event
      ?
        (event.code == 101
        ?
          <LinkExpiredView key='link-expired-shown'/>
        :
          <EventView ref='event-view' onSoundOnOff={this.onSoundOnOff} onClickRestricted = {this.onClickRestricted} event_info = {event} 
            current_loc={{latitude: this.state.latitude, longitude: this.state.longitude}}  onEmbed={this.onOpenEmbed}/>
        )
      :
        null;
    let embed_view = event
      ?
        (event.code == 101
        ?
          null
        :
          <EmbedView ref='embed-view' event_info = {event} mute = {this.state.embed_sound_mute}/>
        )
      :
        null;
    return (
      <div className='event-page'>
        <Helmet
         meta={[
            {"property": "fb:app_id", "content": "476059569260293"},
            {"property": "og:url", "content": "http://potluckapp.net/"+(event && event.objectId)},
            {"property": "og:title", "content": "Potluck Video"},
            {"property": "og:description", "content": event && event.caption},
            {"property": "og:site_name", "content": "Potluck"},
            {"property": "og:image", "content": event && event.thumbnail && event.thumbnail['_url']}
          ]}/>
        <Header className='fixed'/>
        <div className='center-page'>
          {events_view}
          <div className={'embed-view-wrapper'+(this.state.embed?' embed':'')} onClick={this.onCancelEmbed}>
            {embed_view}
          </div>
        </div>
        <BottomMenus/>
        {restrict_overlay}
      </div>
    );
  }

});
