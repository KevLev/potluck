var React = require('react');
import Header from '../../views/HeaderView';
import EventView from '../../views/EventView';
import Parse from 'parse';
var ReactDOM = require('react-dom');
var curl = require('curlrequest');
 

import {ScrollListenerMixin} from 'react-scroll-components';

// If you are going to be using stores, be sure to first load in the `Fluxxor`
// module.
//
//     var Fluxxor = require('Fluxxor');
//
// If you want to leverage the use of stores, a suggestion would be to
// initialize an object, and set it to a `stores` variable, and adding a new
// instance of the store as a property to the object, like so:
//
//     var stores = {
//       SomeStore: new SomeStore()
//     };
//
// And also, because we are using the Flux architecture, you may also initialize
// an object full of methods that represent "actions" that will be called upon
// by a "dispatcher", like so:
//
//     var actions = {
//       doSomething: function (info) {
//         this.dispatch('DO_SOMETHING', {info: info});
//       }
//     };
//
// And finally, you would pass the stores and actions to our dispatcher, like
// so:
//
//     var flux = new Fluxxor.Flux(stores, actions);
//
// And, then, you would pass in the reference of your dispatcher to the view
// relies on the dispatcher (that view is returned by the `render` method), like
// so:
//
//     <SomeView flux={flux} />

module.exports = React.createClass({

  mixins: [ScrollListenerMixin],

  getInitialState: function() {
    const APPLICATION_ID = "BfwANODEExfcO23SduQLMdSpuXIIxrOloCm65EWi";
    const JAVASCRIPT_KEY = "94OyYy1qVBQz6clqPI7bzUHAl02oFtTUrN7KwfNU";
    var latitude,longitude;
    Parse.initialize(APPLICATION_ID, JAVASCRIPT_KEY);
    Parse.Cloud.run('NearbyEvents', { latitude: CurrentLocation.latitude, longitude: CurrentLocation.longitude, distanceInMiles: 10000 }, {
      success: (data) => {
        this.setState({
          events: data,
          isLoading: false
        });
      },
      error: function(error) {
      }
    });
    return {
      restrict_clicked: false,
      latitude: CurrentLocation.latitude,
      longitude: CurrentLocation.longitude,
      events: [],
      isLoading: true
    };

  },

  onBlur: function(){
    const {events} = this.state;
    for (var i = 0; i < events.length; i++)
      this.refs['event-view-'+events[i].objectId].focusChanged(false);
  },
  
  onClickRestricted: function(){
    this.setState({
      restrict_clicked: true
    });
  },

  onClickOutsideRestrictedPopup: function(){
    this.setState({
      restrict_clicked: false
    });
  },

  onPageScrollEnd: function() {

    const {events} = this.state;
    var min_distance_index = -1;
    var min_distance = -1;
    var min_found = false;
    for (var i = 0; i < events.length; i++) {
      var element = this.refs['event-view-'+events[i].objectId];
      var dom_element = ReactDOM.findDOMNode(element);
      var rect = dom_element.getBoundingClientRect();
      var distance = Math.abs(window.innerHeight/2-(rect.top+rect.bottom)/2);  
      if(distance < min_distance || min_distance == -1)
      {
        min_distance_index = i;
        min_distance = distance;
      }
    };
    for (var i = 0; i < events.length; i++)
      this.refs['event-view-'+events[i].objectId].focusChanged(i == min_distance_index);
    
  },

  onSoundOnOff: function(e){
    e.stopPropagation();
    const {events} = this.state;
    for (var i = 0; i < events.length; i++)
      this.refs['event-view-'+events[i].objectId].onSoundOnOff();
  },

  componentDidMount: function(){
    this.onPageScrollEnd();
    window.onblur = this.onBlur;
    window.onfocus = this.onPageScrollEnd;
  },

  componentDidUpdate: function(){
    this.onPageScrollEnd();
  },

  render: function () {
    const {restrict_clicked, events, latitude, longitude, isLoading} = this.state;
    let restrict_overlay = restrict_clicked 
      ?
        <div className='event-restrict-overlay'>
          <div className='overlay' onClick={this.onClickOutsideRestrictedPopup}/>
          <div className='popup'>
            <div className='title'>Uh Oh</div>
            <div className='description'>
              This feature is only available in the mobile app.<br/><br/>
              Please download the free Potluck App for full access.<br/><br/>
            </div>
            <img src='/assets/img/appstore-download-green.png' className='appstore-download'/>
            <img src='/assets/img/download-popup-close.png' className='close-button' onClick={this.onClickOutsideRestrictedPopup}/>
          </div>
        </div>
      :
        null;
    if (restrict_clicked){
      document.body.style.overflow = "hidden";
    }else{
      document.body.style.overflow = "auto";
    }

    let events_views = events.length
      ?
        events.map((event, index) => {
          return <EventView key={'event-view-'+event.objectId} location={{latitude: latitude, longitude: longitude}} ref={'event-view-'+event.objectId} onSoundOnOff={this.onSoundOnOff} 
          onClickRestricted = {this.onClickRestricted} event_info = {event} current_loc={{latitude: this.state.latitude, longitude: this.state.longitude}}/>
        })
      :
      <div className='events-status'>{isLoading?"Loading ...":"No events around you."} </div>
    return (
      <div className='nearby-page'>
        <Header title="Nearby Events"/>
        {events_views}
        {restrict_overlay}
      </div>
    );
  }

});
