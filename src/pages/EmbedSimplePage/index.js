var React = require('react');
var helpers = require('../../helpers');

import {ScrollListenerMixin} from 'react-scroll-components';
import { History } from 'react-router';
import Parse from 'parse';
var http = require("http");
var url = require("url");

// If you want to use a store for this view, you first load up a store:
//
//     var FooStore = require('../../stores/FooStore');
//
// You would then get the mixin to add functionality to our view that will best
// interact with our store:
//
//     var Fluxxor = require('Fluxxor');
//     
//     var StoreWatchMixin = Fluxxor.StoreWatchMixin;

module.exports = React.createClass({
  
  mixins: [ScrollListenerMixin, History],

  getInitialState: function() {

    return {
      'mute': false,
      event: null
    };

  },

  componentDidMount: function() {
    const APPLICATION_ID = "BfwANODEExfcO23SduQLMdSpuXIIxrOloCm65EWi";
    const JAVASCRIPT_KEY = "94OyYy1qVBQz6clqPI7bzUHAl02oFtTUrN7KwfNU";
    Parse.initialize(APPLICATION_ID, JAVASCRIPT_KEY);

    Parse.Cloud.run('Event', { id: this.props.params.objectId }, {
      success: (data) => {
        this.setState({
          event: data
        });
        this.loadVideo(data);
      },
      error: (error) => {
        this.setState({
          event: {code: 101}
        });
      }
    });
  },

  loadVideo: function(event){
    const {picture} = event;
    let video_element = this.refs['event-video'];
    var parser = document.createElement('a');
    parser.href = picture['_url'];
    var options = {
      host: parser.host,
      path: parser.pathname,
    };
    var req = http.get(options,  (res) => {
      if (res.statusCode !== 200) {
        // handle error
        return;
      }
      
      var data = [], dataLen = 0;
      res.on("data", (chunk) => {
        data.push(chunk);
        dataLen += chunk.length;
      });

      res.on("end", () => {
        var buf = new Buffer(dataLen);
        for (var i=0,len=data.length,pos=0; i<len; i++) {
          data[i].copy(buf, pos);
          pos += data[i].length;
        }

        var blobObject = new Blob([new Uint8Array(buf)], { type: 'video/mp4' }); 

        var src = window.URL.createObjectURL(blobObject);
        video_element.src = src;
        video_element.load();
        video_element.play();
      });
    });
  },

  onSwitch: function(is_on) {
    console.log(is_on);
  },

  render: function () {
    const {mute} = this.state;
    const {thumbnail} = (this.state && this.state.event) || {thumbnail: {}};
    let sound_icon_src = mute?'/assets/img/sound-off.png':'/assets/img/sound-on.png';

    return (
      <div className='embed-simple-page'>
        <div className='event-video-box' onClick={this.onVideoBoxClick}>
          <video ref='event-video' poster={thumbnail['_url']} loop preload='auto' className='event-video'>
            Your browser does not support the video tag.
          </video>
          <img className='sound-onoff' src={sound_icon_src} onClick={this.props.onSoundOnOff}/>
        </div>
      </div>
    );
  }
});
