const initialState = {
	request_type : "",
	request: {
		submitting: false,
		succeed: false,
		redirect_url : "",
		errors: []
	}
}
export default function userReducer(state = initialState, action){
	let newState = Object.assign({}, state);

	const {data, type, request_type} = action;
	newState = Object.assign(newState, {request_type : request_type || ""});
	switch(type){
		//Register Processes
		case 'REGISTER_REQUESTED':
		case 'LOGIN_REQUESTED':
		case 'GOOGLE_LOGIN_REQUESTED':		
			newState.request = Object.assign(newState.request, {
				submitting : true,
				succeed : false,
				redirect_url : "",
				errors: []
			});
			return newState;
		case 'REGISTER_SUCCEED':
		case 'LOGIN_SUCCEED':
		case 'GOOGLE_LOGIN_SUCCEED':
			newState.request = Object.assign({}, newState.request, {
				submitting : false,
				succeed : true,
				redirect_url : request_type=='google_login'?data.service_info.url:"",
				errors: []
			});
			return newState;
		case 'REGISTER_FAILED':
		case 'LOGIN_FAILED':
		case 'GOOGLE_LOGIN_FAILED':
			newState.request = Object.assign({}, newState.request, {
				submitting : false,
				succeed : false,
				redirect_url : "",
				errors: data.error.slice()
			});
			return newState;
		default:
			return newState;
	}
}