import { combineReducers } from 'redux';
import main from './MainReducer';
import user from './UserReducer';

const Reducer = combineReducers({
	main, user
})

export default Reducer