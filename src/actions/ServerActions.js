import request from 'superagent';
const api_endpoint = "http://build.atomicgs.com:8080/v1/"
export function test_api_function(){
	
}

export function register(data){
	return dispatch  => {
		dispatch({
			type : 'REGISTER_REQUESTED',
			request_type : "register",
			data
		});
		let param = JSON.stringify(
		{
			"service_params": data
		});
		return request
			.post(api_endpoint + 'register')
			.send(param)
			.set('Accept', 'application/json')
			.end(function(err, res){
				const {body} = res;
				err || body.status == "error"?
					dispatch(register_failed(err || body)) :
					dispatch(register_succeed(body))
			})
	}
}

export function register_failed(data){
	return dispatch => {
		dispatch({
			type : 'REGISTER_FAILED',
			request_type : "register",
			data
		});
	}
}

export function register_succeed(data){
	return dispatch => {
		dispatch({
			type : 'REGISTER_SUCCEED',
			request_type : "register",
			data
		});
	}
}

export function login(data){
	return dispatch  => {
		dispatch({
			type : 'LOGIN_REQUESTED',
			request_type : "login",
			data
		});
		let param = JSON.stringify(
		{
			"service_params": data
		});
		return request
			.post(api_endpoint + 'login')
			.send(param)
			.set('Accept', 'application/json')
			.end(function(err, res){
				const {body} = res;
				err || body.status == "error"?
					dispatch(login_failed(err || body)) :
					dispatch(login_succeed(body))
			})
	}
}

export function login_failed(data){
	return dispatch => {
		dispatch({
			type : 'LOGIN_FAILED',
			request_type : "login",
			data
		});
	}
}

export function login_succeed(data){
	return dispatch => {
		dispatch({
			type : 'LOGIN_SUCCEED',
			request_type : "login",
			data
		});
	}
}

export function google_login(data){
	return dispatch  => {
		dispatch({
			type : 'GOOGLE_LOGIN_REQUESTED',
			request_type : "google_login",
			data
		});
		return request
			.get(api_endpoint + 'oauth/get_redirecting_url/google')
			.set('Accept', 'application/json')
			.end(function(err, res){
				const {body} = res;
				console.log(body);
				err || body.status == "error"?
					dispatch(google_login_failed(err || body)) :
					dispatch(google_login_succeed(body))
			})
	}
}

export function google_login_failed(data){
	return dispatch => {
		dispatch({
			type : 'GOOGLE_LOGIN_FAILED',
			request_type : "google_login",
			data
		});
	}
}

export function google_login_succeed(data){
	return dispatch => {
		dispatch({
			type : 'GOOGLE_LOGIN_SUCCEED',
			request_type : "google_login",
			data
		});
	}
}